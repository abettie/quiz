let targets = [];
let current_id = 0;
let roulette = false;
let stopped = false;
let slowdown_count = 0;
let dom_selected_name = null;

let quiz = [];

// 名前ルーレットメイン描画ループ
function loop() {
    if (roulette) {
        if (stopped) {
            slowdown_count += 1;
        }
        if (!stopped || $.inArray(slowdown_count, [3,5,9,17,33]) >= 0) {
            current_id = Math.floor(Math.random()*targets.length);
            dom_selected_name.text(targets[current_id]+"さん");
            if (slowdown_count === 33) {
                // 決定！
                dom_selected_name.addClass("selected");
            }
        }
    }
    setTimeout("loop()", 100);
}

// ルーレットの状態リセット
function reset_roulette () {
    dom_selected_name.removeClass("selected");
    dom_selected_name.empty();
    slowdown_count = 0;
    roulette = false;
    stopped = false;
}

// ルーレットスタート
function start_roulette () {
    reset_roulette();
    roulette = true;
}

// ルーレットストップ
function stop_roulette () {
    stopped = true;
}

// クイズリストを作成
function setup_quiz_list () {
    let quiz_list = $("#quiz-list");
    quiz.forEach(function (val,idx,arr) {
        quiz_list.append("<button data-num='"+idx+"' class='quiz-no-button btn btn-outline-info'>クイズ"+(idx+1)+"</button>");
    });
    $("button.quiz-no-button").click(function () {
        let quiz_idx = $(this).data("num");
        let target_quiz = quiz[quiz_idx];
        $("#quiz-body").html(target_quiz.body);
        target_quiz.choices.forEach(function (val,idx,arr) {
            let choice_box = $("<div>").addClass("choice-box");
            if (val === "png" || val === "jpg") {
                let url = "./img/quiz/"+(quiz_idx+1)+"_"+(idx+1)+"."+val;
                choice_box.append("<div><span>"+(idx+1)+". </span><img src='"+url+"'></div>");
            } else {
                choice_box.append("<div><span>"+(idx+1)+". </span>"+val+"</div>");
            }
            dom_choice_list.append(choice_box);
        });
        hide_all_page();
        dom_quiz_page.removeClass("hidden");
    });
}

// 全ての画面を非表示
function hide_all_page () {
    dom_roulette_page.addClass("hidden");
    dom_quiz_page.addClass("hidden");
    dom_quiz_select_page.addClass("hidden");
}

// クイズの状態をリセット
function reset_quiz () {
    $("#quiz-body").empty();
    dom_choice_list.empty();
}

// DOM読み込み待ち
$(function () {
    //よく使うDOMを変数に格納
    dom_button_to_quiz_select = $(".button-to-quiz-select");
    dom_selected_name = $("#selected-name");
    dom_roulette_page = $("#roulette-page");
    dom_quiz_select_page = $("#quiz-select-page");
    dom_quiz_page = $("#quiz-page");
    dom_choice_list = $("#choice-list");
    dom_button_start = $("#button-start");
    dom_button_stop = $("#button-stop");
    dom_button_start2 = $("#button-start2");
    dom_button_stop2 = $("#button-stop2");
    // 名前ルーレット
    $.getJSON("data/targets.json", function (data) {
        targets = data;
        loop();
    });
    dom_button_start.click(function () {
        start_roulette();
    });
    dom_button_stop.click(function () {
        stop_roulette();
    });
    dom_button_start2.click(function () {
        start_roulette();
        dom_button_start2.addClass("hidden");
        dom_button_stop2.removeClass("hidden");
    });
    dom_button_stop2.click(function () {
        stop_roulette();
        dom_button_stop2.addClass("hidden");
        dom_button_start2.removeClass("hidden");
    });
    dom_button_to_quiz_select.click(function () {
        hide_all_page();
        reset_quiz();
        reset_roulette();
        dom_quiz_select_page.removeClass("hidden");
    });
    $(".button-to-roulette").click(function () {
        hide_all_page();
        reset_quiz();
        dom_roulette_page.removeClass("hidden");
    });
    // クイズマスター取得
    $.getJSON("data/quiz.json", function (data) {
        quiz = data;
        setup_quiz_list();
    });
});



